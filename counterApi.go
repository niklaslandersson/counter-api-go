package main

import (
   "fmt"
   "encoding/json"
   "log"
   "net/http"

   "github.com/gorilla/mux"
)

var m = make(map[string]int)

func main() {

   router := mux.NewRouter().StrictSlash(true)
   router.HandleFunc("/getValue/{counterName}", getValue)
   router.HandleFunc("/incrementCounter/{counterName}", incrementCounter)
   router.HandleFunc("/getCounters/", getCounters)

   log.Fatal(http.ListenAndServe(":8080", router))
}

/*
* Gets the value of counter specified in the URL
*/
func getValue(w http.ResponseWriter, r *http.Request) {
   vars := mux.Vars(r)
   counterName := vars["counterName"]
   value := m[counterName]
   fmt.Fprintln(w, "Current value of counter", counterName," is ", value, ".")
}

/*
* Increments the counter specified in the URL by one.
*/
func incrementCounter(w http.ResponseWriter, r *http.Request) {
   vars := mux.Vars(r)
   counterName := vars["counterName"]
   value := m[counterName]
   newValue := value + 1
   m[counterName] = newValue
   fmt.Fprintln(w, "Counter", counterName, "was incremented. New value is", newValue, ".")
}

/*
* Returns a reponse containing a list of all counters in the form of a JSON object.
*/
func getCounters(w http.ResponseWriter, r *http.Request) {
   jsonString, err := json.Marshal(m)
   if err != nil {
      fmt.Fprintln(w, err)
   } else {
         fmt.Fprintln(w, string(jsonString))
   }
}
